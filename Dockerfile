FROM alpine AS base

FROM base AS build-base
RUN apk update && \
    apk add \
    curl

FROM build-base AS downloader
WORKDIR /downloads

RUN curl -fL https://storage.googleapis.com/kubernetes-release/release/v1.15.12/bin/linux/amd64/kubectl -o kubectl && \
    chmod +x kubectl
RUN curl -fL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v4.0.5/kustomize_v4.0.5_linux_amd64.tar.gz | tar xz && \
    chmod +x kustomize

# Runtime
FROM base AS runtime

LABEL maintainer Kamal Dodiya <kamal@ebi.ac.uk>

RUN apk --update add git less openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

COPY --from=downloader /downloads/kubectl /usr/local/bin/kubectl
COPY --from=downloader /downloads/kustomize /usr/local/bin/kustomize

CMD ["/bin/sh"]